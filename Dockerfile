FROM java:8-jdk
COPY target/*.jar /home/kaushal/Downloads/springDemo.jar
WORKDIR /home/kaushal/Downloads
EXPOSE 8080
CMD ["java","-jar","/home/kaushal/Downloads/springDemo.jar"]