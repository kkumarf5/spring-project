package com.demo.spring.data;

public interface SomeDataService {

	int[] retrieveAllData();
	
	//int retrieveSpecificData();

}
