package com.demo.spring.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.spring.model.Item;

public interface ItemRepository extends JpaRepository<Item, Integer>{

}
